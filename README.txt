This module uses the JasperReport Server REST API to integrate Drupal with
JasperReports Server.

The SSO method uses JasperServer's REST API and cookies for the allowing
the user to login (initiate a session) in Jasper when he logs into Drupal.
(For this to work JRS and the Drupal have to run in the same domain.)

The JasperServer Session is kept alive on each page refresh and then is
destroyed when the user logs out of Drupal.
