<?php
/**
 * @file
 * This file includes Helper Functions for accesing the Jasperserver REST API.
 */

define('JRS_REST_URL', 'rest/');
define('JRS_RESTV2_URL', 'rest_v2/');

/**
 * Return the proper JasperServer Base URL for either REST or JRS HTTP API.
 */
function jasperreports_server_sso_url() {
  $port = variable_get('jasperreports_server_sso_port', 8080);
  $context = variable_get(
    'jasperreports_server_sso_context',
    'jasperserver-pro'
  );
  return $GLOBALS['base_root'] . ':' . $port . '/' . $context;
}

/**
 * Rest call to keep the JRS session alive.
 */
function jasperreports_server_sso_rest_keepalive() {

  $curl_opts = array();
  $curl_opts[CURLOPT_COOKIE] = $_SESSION['JRS_Cookie'];
  $response = jasperreports_server_sso_rest_doRequest(jasperreports_server_sso_url() . '/' . JRS_REST_URL . 'resources?limit=1', $curl_opts);

  return $response['metadata']['http_code'];

}

/**
 * Rest call to post data.
 */
function jasperreports_server_sso_rest_post($service, $data, $jrs_rest_url = '', $headers = array()) {
  if ($jrs_rest_url == '') {
    $jrs_rest_url = jasperreports_server_sso_url() . '/' . JRS_REST_URL;
  }

  $request_data = http_build_query($data);
  $headers[] = 'Content-Length: ' . strlen($request_data);

  $curl_opts = array();
  $curl_opts[CURLOPT_CUSTOMREQUEST] = 'POST';
  $curl_opts[CURLOPT_HTTPHEADER] = $headers;
  $curl_opts[CURLOPT_POSTFIELDS] = $request_data;

  return jasperreports_server_sso_rest_doRequest($jrs_rest_url . $service, $curl_opts);
}

/**
 * REST request handler.
 */
function jasperreports_server_sso_rest_doRequest($url, $curl_opts) {
  $std_opts = array(
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_SSL_VERIFYPEER => FALSE,
    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_HEADER => TRUE,
  );

  $curl = curl_init($url);
  foreach ($std_opts as $opt => $val) {
    curl_setopt($curl, $opt, $val);
  }
  foreach ($curl_opts as $opt => $val) {
    curl_setopt($curl, $opt, $val);
  }

  $response['body'] = curl_exec($curl);
  $response['metadata'] = curl_getinfo($curl);
  curl_close($curl);
  return $response;
}
